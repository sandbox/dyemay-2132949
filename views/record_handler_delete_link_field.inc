<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying deletes links
 * as fields
 */


class record_handler_delete_link_field extends record_handler_link_field {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }


  function render($values) {
    $type = $values->{$this->aliases['type']};

    //Creating a dummy record to check access against
    $dummy_record = (object) array('type' => $type);
    if (!record_access('edit', $dummy_record)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    $rid = $values->{$this->aliases['rid']};

    return l($text, 'admin/content/records/record/' . $rid . '/delete');
  }
}
