<?php

/**
 * @file
 * Record editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class RecordUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {

    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Records',
      'description' => 'Add edit and update records.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the overview menu type for the list of records.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;

    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add a record',
      'description' => 'Add a new record',
      'page callback'  => 'record_add_page',
      'access callback'  => 'record_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'record.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])

    );

    // Add menu items to add each different type of entity.
    foreach (record_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type] = array(
        'title' => 'Add ' . $type->label,
        'page callback' => 'record_form_wrapper',
        'page arguments' => array(record_create(array('type' => $type->type))),
        'access callback' => 'record_access',
        'access arguments' => array('edit', 'edit ' . $type->type),
        'file' => 'record.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module'])
      );
    }

    // Loading and editing record entities
    $items[$this->path . '/record/' . $wildcard] = array(
      'page callback' => 'record_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'record_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'record.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items[$this->path . '/record/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );

    $items[$this->path . '/record/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'record_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'record_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'record.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    // Menu item for viewing records
    $items['record/' . $wildcard] = array(
      //'title' => 'Title',
      'title callback' => 'record_page_title',
      'title arguments' => array(1),
      'page callback' => 'record_page_view',
      'page arguments' => array(1),
      'access callback' => 'record_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );
    return $items;
  }


  /**
   * Create the markup for the add Record Entities page within the class
   * so it can easily be extended/overriden.
   */
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }

    return theme('record_add_list', array('content' => $content));
  }

}


/**
 * Form callback wrapper: create or edit a record.
 *
 * @param $record
 *   The record object being edited by this form.
 *
 * @see record_edit_form()
 */
function record_form_wrapper($record) {
  // Add the breadcrumb for the form's location.
  record_set_breadcrumb();
  return drupal_get_form('record_edit_form', $record);
}


/**
 * Form callback wrapper: delete a record.
 *
 * @param $record
 *   The record object being edited by this form.
 *
 * @see record_edit_form()
 */
function record_delete_form_wrapper($record) {
  // Add the breadcrumb for the form's location.
  //record_set_breadcrumb();
  return drupal_get_form('record_delete_form', $record);
}


/**
 * Form callback: create or edit a record.
 *
 * @param $record
 *   The record object to edit or for a create form an empty record object
 *     with only a record type defined.
 */
function record_edit_form($form, &$form_state, $record) {

	$q=db_select('record_type', 't');
	$q->fields('t');
	$q->condition('type', $record->type);
	$settings=$q->execute()->fetchObject();
  $settings->data=unserialize($settings->data);
	$form_state['storage']['settings']=$settings;

  // Add the default field elements.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => isset($settings->data['title']) ? t($settings->data['title']) : t('Record Name'),
    '#default_value' => isset($record->name) ? $record->name : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  // Add the field related form elements.
  $form_state['record'] = $record;
  field_attach_form('record', $record, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save record'),
    '#submit' => $submit + array('record_edit_form_submit'),
  );

  if (!empty($record->name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete record'),
      '#suffix' => l(t('Cancel'), 'admin/content/records'),
      '#submit' => $submit + array('record_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'record_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the record form
 */
function record_edit_form_validate(&$form, &$form_state) {
  $record = $form_state['record'];

  $vals = (object) $form_state['values'];
	
	$settings = $form_state['storage']['settings'];

	if (isset($settings->data['unique']) && $settings->data['unique']){
		$query = db_select('record', 't')
			->fields('t', array('rid'))
			->condition('type', $record->type)
			->condition('name', $vals->name);
		if($record->rid) $query->condition('rid', $record->rid, '<>');
		if ($query->execute()->fetchObject()){
				form_set_error('name', t('The @name field shall be unique. Record with such @name already exists.',
					array('@name'=> isset($settings->data['title']) ? t($settings->data['title']) : t('Record Name'))));
			}
	}

  // Notify field widgets to validate their data.
  field_attach_form_validate('record', $record, $form, $form_state);
}


/**
 * Form API submit callback for the record form.
 *
 * @todo remove hard-coded link
 */
function record_edit_form_submit(&$form, &$form_state) {

  $record = entity_ui_controller('record')->entityFormSubmitBuildEntity($form, $form_state);
  // Save the record and go back to the list of records

  // Add in created and changed times.
  if ($record->is_new = isset($record->is_new) ? $record->is_new : 0){
    $record->created = time();
  }

  $record->changed = time();

  if($record->save())
	drupal_set_message(t('Records saved successfully'));
  else
	drupal_set_message(t('Failed to save record'), 'error');

  $form_state['redirect'] = 'admin/content/records';
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function record_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/records/record/' . $form_state['record']->rid . '/delete';
}


/**
 * Form callback: confirmation form for deleting a record.
 *
 * @param $record
 *   The record to delete
 *
 * @see confirm_form()
 */
function record_delete_form($form, &$form_state, $record) {
  $form_state['record'] = $record;

  $form['#submit'][] = 'record_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete record %name?', array('%name' => $record->name)),
    'admin/content/records/record',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for record_delete_form
 */
function record_delete_form_submit($form, &$form_state) {
  $record = $form_state['record'];

  record_delete($record);

  drupal_set_message(t('The record %name has been deleted.', array('%name' => $record->name)));
  watchdog('record', 'Deleted record %name.', array('%name' => $record->name));

  $form_state['redirect'] = 'admin/content/records';
}



/**
 * Page to add Record Entities.
 *
 * @todo Pass this through a proper theme function
 */
function record_add_page() {
  $controller = entity_ui_controller('record');
  return $controller->addPage();
}


/**
 * Displays the list of available record types for record creation.
 *
 * @ingroup themeable
 */
function theme_record_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="record-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer record types')) {
      $output = '<p>' . t('Record Entities cannot be added because you have not created any record types yet. Go to the <a href="@create-record-type">record type creation page</a> to add a new record type.', array('@create-record-type' => url('admin/structure/record_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No record types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}





/**
 * Sets the breadcrumb for administrative record pages.
 */
function record_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('Records'), 'admin/content/records'),
  );

  drupal_set_breadcrumb($breadcrumb);
}



