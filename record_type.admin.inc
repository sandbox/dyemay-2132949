<?php

/**
 * @file
 * Record type editing UI.
 */

/**
 * UI controller.
 */
class RecordTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage record entity types, including adding
		and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the record type editing form.
 */
function record_type_form($form, &$form_state, $record_type, $op = 'edit') {

  if ($op == 'clone') {
    $record_type->label .= ' (cloned)';
    $record_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $record_type->label,
    '#description' => t('The human-readable name of this record type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($record_type->type) ? $record_type->type : '',
    '#maxlength' => 32,
//    '#disabled' => $record_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'record_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this record type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['data']['#tree'] = TRUE;
	$form['data']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($record_type->data['title']) ? $record_type->data['title'] : "Record title",
		'#description' => t('The title of basic field.'),
		'#size' => 30,
		 '#maxlength' => 35,
  );
	$form['data']['unique'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unique'),
    '#default_value' => !empty($model_type->data['sample_data']),
		'#description' => t('Make title field values unique for this record type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save record type'),
    '#weight' =>40,
  );

  //Locking not supported yet
  /*if (!$record_type->isLocked() && $op != 'add') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete record type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('record_type_form_submit_delete')
    );
  }*/
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function record_type_form_submit(&$form, &$form_state) {
  $record_type = entity_ui_form_submit_build_entity($form, $form_state);
  $record_type->save();
  $form_state['redirect'] = 'admin/structure/record_types';
}

/**
 * Form API submit callback for the delete button.
 */
function record_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/record_types/manage/' . $form_state['record_type']->type . '/delete';
}
